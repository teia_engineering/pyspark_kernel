# Instruction to Start Development
If you are going to start developing for the library here is some helpful information

## Environment and Pre-Requisites
At the time of the writing, the development environment used is `Ubuntu 20`. Since this is a Kernel to use with Spark you also need to have Spark installed and running on the environment. For installation I followed the following tutorial.

    https://phoenixnap.com/kb/install-spark-on-ubuntu

Once that is down install Python(3.8) and Jupyter notebook.

## Installing the package
Assuming git is installed in the environment, clone the project from the repository

    git clone git@gitlab.com:teia_engineering/pyspark_kernel.git

Once you clone and go into that project directory. Once there install the package by running the following command.

    pip install pyspark_kernel

After that you got to install the library as a Kernel for jupyter by running the followign command

    python3 -m pyspark_kernel install --user

## Changing and updating the library as you code
Since we installed the library from the directory, if you update the code from the directory and restart the kernel int he notebnook itwill run wkththe new code.


